@extends('layouts.app')

@section('title', 'TESTE CARBONO')
@section('stylesheets')
@endsection

@section('content')
<div class="container search">
    <div class="row">
        <div class="col-sm-12">
            <div class="input-group">
                    <label for="">Busca</label>
                    <input id="btn_pesquisa" name="btn_pesquisa" type="text" class="form-control" placeholder="Termo pesquisa">
            </div>
           
        </div>
        
        
    </div>  
</div>
<div class="container">
  <!-- inject content here  -->
  <div class="row imoveis" id="container_imoveis">

      <style>
          .sprite td{          
            border-top-left-radius:20px;
            border-top-right-radius:20px;
            border-bottom-right-radius:20px;
            border-bottom-left-radius:20px;
          }
      </style>
      
    </div>
</div>

@endsection

@push('scripts_bottom')
<script type="text/javascript">
var template=
      '<div class="template_imovel col-lg-4 col-md-4 col-sm-4 col-xs-4">'      
      +'<div style="">'
      +'<div class="row">'
      +'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nome paddingfix">%%NOME%%</div>'
      +'</div>'
      +'<div class="row">'
      +'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bairro paddingfix">%%BAIRRO%%</div>'
      +'</div>'
      +'</div>'
      +'<div class="item_imovel col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">'
      +'<div class="row" >'
      +'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="min-height:272px; text-align:center;padding:15px;background:white;border-radius:15px;">'
      +'<div class="row">'
      +'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="position: relative;">'
      +'%%ESCONDEINIT%%<div class="absolute">'
        +'<div class="absolute-text">'
	+'DESTAQUE'
	+'</div>'
	+'<img src="{{asset("destaque_risquinha.png")}}">' 
	+'</div>%%ESCONDEFINIT%%' 
        +'<table class="sprite" width="100%" height="100%">'  
        +'<tr valign="middle">' 
        +'<td align="center"><img class="img1" style="max-width:100%;height:auto" src="%%IMG1%%" /></td>' 
        +'<td align="center"><img class="img2" style="max-width:100%;height:auto" src="%%IMG2%%" /></td>'
        +'</tr>'
        +'<tr valign="middle">'
        +'<td align="center"><img class="img3" style="max-width:100%;height:auto" src="%%IMG3%%" /></td>' 
        +'<td align="center"><img class="img4" style="max-width:100%;height:auto" src="%%IMG4%%" /></td>' 
        +'</tr>'
        +'</table>'
        +'</div>'
        +'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingfix" style="">'
        +'<hr \/>'
        +'</div>'
        +'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingfix" style="">'
        +'<div class="row margin0" >'
        +'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 medida2" style="padding:0px;">%%MEDIDA%%</div>'
        +'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 valor" style="padding:0px;">%%VALOR%%</div>'
        +'</div>'
        +'<div class="row margin0">'
        +'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 codigo" style="padding:0px;">%%CODIGO%%</div>'
        +'</div>'
        +'</div>'
        +'</div>'
        +'</div>'  
        +'</div>'
        +'</div>'
        +'</div>'; 
function insere_imovel(imovel=null){
    
    if(imovel==null) return false;
    console.log(imovel.nome)
        console.log($(".template_listagem").length)    
        var template2=template;;
        console.log(template2);
        template2=template2.replace("%%NOME%%",imovel.nome+", "+imovel.medida);
        template2=template2.replace("%%BAIRRO%%",imovel.bairro+", "+imovel.cidade);
        var contador=1
        jQuery.each(imovel.imagens, function() {
                       console.log($(this)[0].file); 
                       template2=template2.replace("%%IMG"+contador+"%%",$(this)[0].file);  
                       contador++;
                  });
        if(contador!=5){
            while(contador<5){
                template2=template2.replace("%%IMG"+contador+"%%","{{asset("logo-carbono.png")}}");
                contador++;
            }
        }
		if(imovel.destaque=="on"){
			
			template2=template2.replace("%%ESCONDEINIT%%","");
			template2=template2.replace("%%ESCONDEFINIT%%","");
		}else{
			template2=template2.replace("%%ESCONDEINIT%%","<!--");
			template2=template2.replace("%%ESCONDEFINIT%%","-->");
		}
		
        template2=template2.replace("%%MEDIDA%%",imovel.medida);
        template2=template2.replace("%%VALOR%%","&#82;&#36; "+imovel.valor);
        template2=template2.replace("%%CODIGO%%",imovel.codigo);
        console.log(template2)
        //inserir na listagem
        $("#container_imoveis").append(template2);
}

$(document).ready(function(){
    $.ajax({
      url: "{{asset("jsonformatter.txt")}}",
      success: function(result){
                    jQuery.each(result.imoveis, function() {
                       console.log($(this)[0]); 
                       
                       insere_imovel($(this)[0]);
                  });
                },
      dataType: "json"
});
});
    

</script>
@endpush()

