<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/imoveis/index', 'Imoveis\ImoveisController@index')->name('imoveis.index');
Route::get('/json', function(){
    $json='{"imoveis":{"01":{"destaque":"on","nome":"Apartamento 01","codigo":"4452","medida":"60 m²","valor":"R$ 700,00","bairro":"Centro","cidade":"Fortaleza","imagens":{"01":{"file":"https://via.placeholder.com/150"},"02":{"file":"https://via.placeholder.com/150"},"03":{"file":"https://via.placeholder.com/150"},"04":{"file":"https://via.placeholder.com/150"}}},"02":{"destaque":"off","nome":"Apartamento Novo","codigo":"4402","medida":"40 m²","valor":"R$ 400,00","bairro":"Aldeota","cidade":"Fortaleza","imagens":{"01":{"file":"https://via.placeholder.com/150"}}},"03":{"destaque":"off","nome":"Casa","codigo":"2252","medida":"100 m²","valor":"R$ 1200,00","bairro":"Meireles","cidade":"Fortaleza","imagens":{"01":{"file":"https://via.placeholder.com/150"},"02":{"file":"https://via.placeholder.com/150"},"03":{"file":"https://via.placeholder.com/150"}}},"04":{"destaque":"off","nome":"Sala Comercial","codigo":"2200","medida":"48 m²","valor":"R$ 1250,00","bairro":"Aldeota","cidade":"Fortaleza","imagens":{"01":{"file":"https://via.placeholder.com/150"},"02":{"file":"https://via.placeholder.com/150"},"03":{"file":"https://via.placeholder.com/150"}}},"05":{"destaque":"off","nome":"Apartamento teste","codigo":"61651","medida":"75 m²","valor":"R$ 780,00","bairro":"Dionisio Torres","cidade":"Fortaleza","imagens":{"01":{"file":"https://via.placeholder.com/150"},"02":{"file":"https://via.placeholder.com/150"}}},"06":{"destaque":"on","nome":"Vagas","codigo":"1234","medida":"5 m²","valor":"R$ 200,00","bairro":"Centro","cidade":"Fortaleza","imagens":{"01":{"file":"https://via.placeholder.com/150"}}},"07":{"destaque":"on","nome":"Apartamento praia","codigo":"4455","medida":"140 m²","valor":"R$ 2100,00","bairro":"Cumbuco","cidade":"Caucaia","imagens":{"01":{"file":"https://via.placeholder.com/150"},"02":{"file":"https://via.placeholder.com/150"},"03":{"file":"https://via.placeholder.com/150"},"04":{"file":"https://via.placeholder.com/150"}}},"08":{"destaque":"off","nome":"Casa","codigo":"1121","medida":"30 m²","valor":"R$ 400,00","bairro":"Aldeota","cidade":"Fortaleza","imagens":{"01":{"file":"https://via.placeholder.com/150"},"02":{"file":"https://via.placeholder.com/150"},"03":{"file":"https://via.placeholder.com/150"},"04":{"file":"https://via.placeholder.com/150"}}}}}';
    echo $json;
});